//
//  NewTeamViewController.swift
//  Northwest-KC Contest App
//
//  Created by Student on 3/14/19.
//  Copyright © 2019 sambireddych. All rights reserved.
//

import UIKit

class NewTeamViewController: UIViewController {

    var school:School!
    @IBOutlet weak var teamnameTF: UITextField!
    @IBOutlet weak var student0TF: UITextField!
    @IBOutlet weak var student1TF: UITextField!
    @IBOutlet weak var student2TF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func done(_ sender: Any) {
        let name = teamnameTF.text!
        let student0 = student0TF.text!
        let student1 = student1TF.text!
        let student2 = student2TF.text!
        if (name == "") && (student0 == "" || student1 == "" || student2 == "") {
            displayMessage()
        }
        else{
            let students = [student0,student1,student2]
            let team = Team(name: name, student01: students[0], student02: students[1], student03: students[2])
            Schools.shared.addTeam(team, toSchool: school)
            //school.addTeam(name: team.name!, students: team.students)
//            Schools.shared.add(school: school)
            self.dismiss(animated: true, completion: nil) // instead of using an unwind segue
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil) // instead of using an unwind segue
    }
    
    func displayMessage(){
        let alert = UIAlertController(title: "Warning",
                                      message: "You need to input all values",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default,
                                      handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
