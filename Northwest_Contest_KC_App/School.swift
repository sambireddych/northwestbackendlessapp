//
//  School.swift
//  Northwest-KC Programing Contest App
//
//  Created by Student on 3/13/19.
//  Copyright © 2019 Student. All rights reserved.
//

import Foundation

@objcMembers
class School : NSObject {
    
    var backendless = Backendless.sharedInstance()
    var schooldatastore:IDataStore!
    var name:String?
    var coach:String?
    var teams:[Team] = []

    var objectId:String?
    
    private override init(){
        schooldatastore = backendless!.data.of(School.self)
    }
    //  var selectedSchool:School!
    init(name:String?, coach:String?, teams:[Team]){
        self.name = name
        self.coach = coach
        self.teams = teams
    }
/*    func addTeam(name: String, students: [String]){
        
        
        var teamssave = Team(name: name, students: students)

        teamssave = self.teamDataStore.save(teamssave) as! Team
        print(teamssave)
        self.schooldatastore.addRelation("teams:Team:n", parentObjectId:self.objectId, childObjects: [teamssave.objectId!])
        
        self.teams.append(teamssave)
        
    }*/
    
}
