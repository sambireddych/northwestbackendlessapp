//
//  Schools.swift
//  Northwest-KC Programing Contest App
//
//  Created by Student on 3/13/19.
//  Copyright © 2019 Student. All rights reserved.
//

import Foundation


extension Notification.Name {
    static let SchoolsRetrieved = Notification.Name("schools Retrieved")
    static let TouristSitesForSelectedCityRetrieved = Notification.Name("teams for Selected school Retrieved")
    static let TouristSitesRetrieved = Notification.Name("Teams Retrieved")
}

@objcMembers
class Schools{
    
    
    let backendless = Backendless.sharedInstance()
    var schoolDataStore:IDataStore!
    var teamDataStore:IDataStore!
    static let shared = Schools()
    private var schools:[School]
    var objectid:String?
  
    var teams:[Team] = []
    var selectedSchool:School?
    var teamsForSelectedSchool:[Team] = []
    
    func numSchools() -> Int{
        return schools.count
    }
    subscript(_ index:Int) -> School{
        return schools[index]
    }
    private convenience init(){
        self.init(schools:[])
        schoolDataStore = backendless!.data.of(School.self)
        teamDataStore = backendless!.data.of(Team.self)
    }
    private init(schools:[School]){
        self.schools = schools
    }
    var team:Team!
    func add(school:School){
        schools.append(schoolDataStore.save(school) as! School)
    }
    
    func addTeam(_ team:Team, toSchool school:School){
        let teamssave = teamDataStore.save(team) as! Team
//        print(teamssave.objectId)
        self.schoolDataStore.addRelation("teams:Team:n", parentObjectId:school.objectId, childObjects: [teamssave.objectId!])
        
        school.teams.append(teamssave)
    }
    
    func delete(school:School){
        for i in 0..<schools.count{
            if schools[i].name == school.name{
                schools.remove(at: i)
                let query:DataQueryBuilder = DataQueryBuilder()
                query.setRelated(["teams"])
                let result = schoolDataStore.find(query) as! [School]
                let result1 = result[i].teams
                for i in 0..<result1.count{
                    self.teamDataStore.remove(byId: result1[i].objectId)
                }
                 self.schoolDataStore.remove(byId: school.objectId)
                return
            }
        }
    }
    
    func retreiveAllSchools(){
        let queryBuilder = DataQueryBuilder()
        queryBuilder!.setRelated(["teams"])
        queryBuilder!.setPageSize(100)
        Types.tryblock({() -> Void in
            self.schools = self.schoolDataStore.find(queryBuilder) as! [School]
            
        },
            catchblock: {(fault) -> Void in print(fault ?? "Something has gone wrong  retreiveAllSchools()")}
        )
    }
    
    func retrieveAllTeams() {
        
        Types.tryblock({() -> Void in self.teams = self.teamDataStore.find() as! [Team]}, catchblock: {(fault) -> Void in print(fault ?? "Something has gone wrong when retrieveAllTeams()")})
        
    }
    
    func retrieveTeamsForSelectedSchool() {
        let startDate = Date()
        
        Types.tryblock( {
            let queryBuilder:DataQueryBuilder = DataQueryBuilder()
            queryBuilder.setWhereClause("name = '\(self.selectedSchool?.name ?? "")'" )
            queryBuilder.setPageSize(100)
            queryBuilder.setRelated( ["teams"] )
            let result = self.schoolDataStore.find(queryBuilder) as! [School]
            self.teamsForSelectedSchool = result[0].teams
            },
                        catchblock: {(exception) -> Void in
                            print("Oopsie retrieving tourist sites for selected city -- \(exception.debugDescription)")
        })
        print("Done in \(Date().timeIntervalSince(startDate)) seconds ")
    }
    
}
