//
//  Team.swift
//  Northwest-KC Programing Contest App
//
//  Created by Student on 3/13/19.
//  Copyright © 2019 Student. All rights reserved.
//

import Foundation

@objcMembers
class Team : NSObject {
    var backendless = Backendless.sharedInstance()
    var teamDataStore:IDataStore!
    
    var name:String?
    var student01:String?
    var student02:String?
    var student03:String?
    
    var objectId:String?
    
    init(name:String?, student01:String?, student02:String?, student03:String?){
        self.name=name
        self.student01=student01
        self.student02=student02
        self.student03=student03
    }
    
    private override init(){
        teamDataStore = backendless!.data.of(Team.self)
    }
    
    
}
