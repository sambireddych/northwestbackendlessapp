//
//  NewSchoolViewController.swift
//  Northwest-KC Programing Contest App
//
//  Created by Student on 3/13/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class NewSchoolViewController: UIViewController {
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var coachTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    @IBAction func done(_ sender: Any) {
        let name = nameTF.text!
        let coach = coachTF.text!
        if name == "" || coach == ""  {
            displayMessage()
        }
        else{
            let school = School(name: name, coach: coach, teams: [])
            Schools.shared.add(school: school)
        }
        self.dismiss(animated: true, completion: nil) // instead of using an unwind segue
    }
    
    @IBAction func cancel(_ sender: Any){
        self.dismiss(animated: true, completion: nil) // instead of using an unwind segue
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func displayMessage(){
        let alert = UIAlertController(title: "Warning",
                                      message: "You need to input all values",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default,
                                      handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
