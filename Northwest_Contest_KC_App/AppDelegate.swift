
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    

    let APP_ID = "9204A78D-9267-4A73-FF7B-8809476FBF00"
    let API_KEY = "2CBEB271-3A4F-4CC1-B5FE-D9A25234F900"
    
    var backendless = Backendless.sharedInstance()
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        backendless!.initApp(APP_ID, apiKey: API_KEY)
        return true
    }
}

